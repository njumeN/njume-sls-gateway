import { openInbox } from "./lyftPortalEmailClient";
import { start } from "./lyftPortalGoogleClient";


export const handler = async (event, ctx, cb) => {
    const openInboxRes = await openInbox()
    if (typeof openInboxRes !== 'undefined'){
        const sendToSheetsRes = await start(openInboxRes)
    }
}