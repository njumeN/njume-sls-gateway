Lyft Portal Doc.

1. Every morning an email is sent to our lyft email from Mode (lyft)
2. Google scripts runs every morning, first checks if a new email is sent, then it deletes all rows in google sheet to prepare for new data to be inserted by cron.
3. Cron:
    a - checks if there are new emails from Mode    
    b - parse the data to retrieve driver's email   
    c - query db for driver info based on driver's email
    d - stich queried data together with associated email into multidimensional arrays
    e - split data in half into 2 seperate multidimensional arrays
    c - send send each multidimensional array to seperate sheets