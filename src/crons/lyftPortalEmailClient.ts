import hyrecarGraphql from 'utils/hyrecarGraphql';
import { imaps, imapsConfig } from "utils/lyftPortalHelpers";
import { optionalChaining } from "utils/helpers";

const util = require('util')

export async function openInbox(){
    try {
        const connection = await imaps.connect(imapsConfig)
        const open = await connection.openBox('INBOX')
        const openRes = await openFunction()
        const search = await connection.search(openRes[0],openRes[1])
        const searchRes = await searchFunction(search, connection)
        connection.end()
        if(typeof searchRes !== undefined){
            const massagesDataRes = await massageData(searchRes)
            const massageFilenameRes = await massageFilename(searchRes)
            const parseEmailDataRes = await parseEmailData(massagesDataRes)
            const stitchMessageRes =  await stitchMessage(massagesDataRes,parseEmailDataRes,massageFilenameRes)
            return stitchMessageRes
        }
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }

}
const searchFunction = async (messages, connection) => {
    try{
        const end = messages.length - 1
        let attachments = [];
        let bool = false
        for(let i = end; i>=0; i--){
            if (messages[i].parts[0].body.from[0] == 'Mode <noreply@modeanalytics.com>'){
                const parts = imaps.getParts(messages[i].attributes.struct);
                attachments = attachments.concat(parts.filter( (part) =>{
                    return part.disposition && part.disposition.type.toUpperCase() === 'ATTACHMENT';
                }).map( (part) => {
                    return connection.getPartData(messages[i], part)
                        .then( (partData) => {
                            return {
                                filename: part.disposition.params.filename,
                                data: partData
                            };
                        });
                }));
                bool = true
                break;

            }
        }
        if (bool === true){
            return Promise.all(attachments);
        }
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }

}


const openFunction = function(){
    try{
        const delay = 24 * 3600 * 1000;
        const yesterday = new Date();
        yesterday.setTime(Date.now() - delay);
        const yesterday2 = yesterday.toISOString();
        const searchCriteria = ['UNSEEN', ['SINCE', yesterday2]];
        const fetchOptions = { bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'], struct: true };
        const data = [
            searchCriteria,
            fetchOptions
        ]
        return data
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}

async function parseEmailData(data){
    try {
        const usersArray = [];
        const upperLimit = 1000
        let multiplier = 1
        let count = 0
        const numLoops = data.length/upperLimit
        for(let i =0 ; i<=Math.floor(numLoops); i++ ){
            const driverEmails = [];
            const driverEmailsNoDups = [];
            let userData = []
            data.map( async (value, key, array) => {
                if(key>i*upperLimit && key<=upperLimit*multiplier){
                    const arrayVal = value.split(',');
                    if( !driverEmailsNoDups.includes(arrayVal[2]) ){
                        driverEmailsNoDups.push(optionalChaining(() => arrayVal[2]) || "");
                        count++
                    }
                }
            
            })
            multiplier++
            const variables = {
                emails: driverEmailsNoDups
            }
            userData = await getDriverDetails(variables)
            usersArray.push(userData)  
        }
        console.log(count)
        return usersArray
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}

async function stitchMessage(data, queriedData, fileName){
    try {
        fileName = fileName.replace(/\'/g, '')
        const googleSheetsDataArray = []
        const stichData = []
        for(let i of queriedData){
            if(i.length > 0){
                for(let j of i){
                    stichData.push(j)
                }
            }
        }
        data.map( async (value, key, array) => {
            if(key>0){
                const arrayVal = value.split(',');
                var bool = false
                
                for(let i of stichData){
                    if(arrayVal[2] !== undefined){
                        if(arrayVal[2].toUpperCase() == i.email.toUpperCase()){
                            const googleSheetsData = [
                                optionalChaining(() => i.driver.id) || "N/A",
                                optionalChaining(() => arrayVal[0]) || "N/A",
                                optionalChaining(() => i.firstName) || "N/A",
                                optionalChaining(() => i.lastName) || "N/A",
                                optionalChaining(() => arrayVal[1]) || "N/A",
                                optionalChaining(() => arrayVal[2]) || "N/A",
                                optionalChaining(() => arrayVal[3]) || "N/A",
                                optionalChaining(() => arrayVal[4]) || "N/A",
                                optionalChaining(() => arrayVal[5]) || "N/A",
                                optionalChaining(() => arrayVal[6]) || "N/A",
                                optionalChaining(() => arrayVal[7]) || "N/A",
                                optionalChaining(() => arrayVal[8]) || "N/A",
                                optionalChaining(() => arrayVal[9]) || "N/A",
                                optionalChaining(() => arrayVal[10]) || "N/A",
                                optionalChaining(() => arrayVal[11]) || "N/A",
                                optionalChaining(() => arrayVal[12]) || "N/A",
                                currentDate(),
                                fileName,
                                optionalChaining(() => i.driver.rentals.edges[0].node.status) || "N/A",
                                lyftStage(arrayVal),
                                optionalChaining(() => i.driver.backgroundCheck.verificationStatus) || "N/A"
                            ]
                            googleSheetsDataArray.push(googleSheetsData)
                            bool = true
                        }
                    }
                }
                if (bool === false){
                    const googleSheetsData2 = [
                        "N/A",
                        optionalChaining(() => arrayVal[0]) || "N/A",
                        "N/A",
                        "N/A",
                        optionalChaining(() => arrayVal[1]) || "N/A",
                        optionalChaining(() => arrayVal[2]) || "N/A",
                        optionalChaining(() => arrayVal[3]) || "N/A",
                        optionalChaining(() => arrayVal[4]) || "N/A",
                        optionalChaining(() => arrayVal[5]) || "N/A",
                        optionalChaining(() => arrayVal[6]) || "N/A",
                        optionalChaining(() => arrayVal[7]) || "N/A",
                        optionalChaining(() => arrayVal[8]) || "N/A",
                        optionalChaining(() => arrayVal[9]) || "N/A",
                        optionalChaining(() => arrayVal[10]) || "N/A",
                        optionalChaining(() => arrayVal[11]) || "N/A",
                        optionalChaining(() => arrayVal[12]) || "N/A",
                        currentDate(),
                        fileName,
                        "N/A",
                        lyftStage(arrayVal),
                        "N/A"
                    ]
                    googleSheetsDataArray.push(googleSheetsData2)
                }
            }
        })
        return googleSheetsDataArray
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}

async function massageData(attachments){
    try {
        const file = util.inspect(attachments[0].data.toString('utf-8'), false, null)
        const file2 = file.split("\\n");
        return file2
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}

async function massageFilename(attachments){
    try {
        const fileName = util.inspect(attachments[0].filename.toString('utf-8'), false, null);
        return fileName
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}

//first:1 orderBy:createdAt_DESC
async function getDriverDetails(variables){
    const query = `query fetchDriverFields($emails: [String!]!){
        users(where: {email_in: $emails}){
          id
          email
          firstName
          lastName
          driver{
            id
            backgroundCheck{
                verificationStatus
            }
            rentals(last:1){
              edges{
                node{
                  id
                  status
                }
              }
            }
          }
        }
    }`;
    try {
		const res = await hyrecarGraphql({ query, variables });
        const { users } = res.data;
		return users
	} catch (err) {
		console.error(err);
		throw new Error(err);
	}
}

function currentDate() {
    let now = new Date();
    let year = "" + now.getFullYear();
    let month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    let day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    let hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    let minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    let second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function lyftStage(val){
    try{
        if (val.length > 0){
            let lyft_stage = 5;
            if( (val[6] != 'No') && (val[7] < 10) && (val[7] !== '>10') ){
                lyft_stage = 0;
            }
            else if( (val[10] === 'Yes') && (val[6] === 'No') ){
                lyft_stage = 1;
            }
            else if( (val[10] === 'No') && (val[8] === 'Yes') && (val[9] === 'proceed') && (val[11] === 'proceed') && (val[12] === 'added') && (val[6] === 'No') ){
                lyft_stage = 2;
            }
            else if( (val[10] === 'No') && (val[8] === 'Yes') && (val[9] === 'proceed') && (val[11] === 'proceed') && (val[12] === 'needed') && (val[6] === 'No') ){
                lyft_stage = 3;
            }
            else if( (val[10] === 'No') && (val[8] === 'Yes') && (val[9] === 'flag') && (val[11] === 'proceed') && (val[12] === 'added') && (val[6] === 'No') ){
                lyft_stage = 4;
            }
            
            if (val[7] == '>10'){
                lyft_stage = 5;
            }
            return lyft_stage;
        }
    } catch (err) {
        console.error(err)
        throw new Error(err)
    }
}