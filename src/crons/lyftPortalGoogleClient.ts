import { googleSheets } from "utils/lyftPortalHelpers";
import { google } from 'googleapis';

const fs = require('fs');
const readline = require('readline');

const SCOPES= [
    'https://www.googleapis.com/auth/spreadsheets'
];

export async function start(data){
    const TOKEN_PATH = '../../credentials.json';

    fs.readFile('../../client-secret.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        authorize(JSON.parse(content), sendToSheets);
    });

    function authorize(credentials, callback) {
        const {client_secret, client_id, redirect_uris} = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
    
        fs.readFile(TOKEN_PATH, (err, token) => {
            if (err) return getNewToken(oAuth2Client, callback);
            oAuth2Client.setCredentials(JSON.parse(token));
            callback(oAuth2Client);
        });
    }

    function getNewToken(oAuth2Client, callback) {
        const authUrl = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
        console.log('Authorize this app by visiting this url:', authUrl);
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        rl.question('Enter the code from that page here: ', (code) => {
            rl.close();
            oAuth2Client.getToken(code, (err, token) => {
                if (err) return callback(err);
                oAuth2Client.setCredentials(token);
                // Store the token to disk for later program executions
                fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                    if (err) return console.error(err);
                    console.log('token is: '+JSON.stringify(token))
                    console.log('Token stored to', TOKEN_PATH);
                });
                callback(oAuth2Client);
            });
        });
    }

    const dataOdd = []
    const dataEven = []
    
    data.map( (value, key, array) => {
        if(key % 2 == 0){
            dataEven.push(value)  
        }else{
            dataOdd.push(value)
        }
    })

    function sendToSheets(auth) {
        googleSheets.spreadsheets.values.append({spreadsheetId: process.env.GOOGLE_SHEETS_SHEET_ID,
            range: 'Daniel!A1:U1',
            includeValuesInResponse: false,
            responseValueRenderOption: 'FORMATTED_VALUE',
            insertDataOption: 'INSERT_ROWS',
            valueInputOption: 'USER_ENTERED',
            responseDateTimeRenderOption: 'FORMATTED_STRING',
            resource:{
                values: dataEven
            },
            auth: auth
        }, function(err, response) {
            if (err) {
            console.error(err);
            return;
            }
            console.log(response.status)
        });

        googleSheets.spreadsheets.values.append({spreadsheetId: process.env.GOOGLE_SHEETS_SHEET_ID,
            range: 'Nicole!A1:U1',
            includeValuesInResponse: false,
            responseValueRenderOption: 'FORMATTED_VALUE',
            insertDataOption: 'INSERT_ROWS',
            valueInputOption: 'USER_ENTERED',
            responseDateTimeRenderOption: 'FORMATTED_STRING',
            resource:{
                values: dataOdd
            },
            auth: auth
        }, function(err, response) {
            if (err) {
            console.error(err);
            return;
            }
            console.log(response.status)
        });
    }

    function test(auth){
        console.log(data[0])
    }

}