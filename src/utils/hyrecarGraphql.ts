import { createApolloFetch } from "apollo-fetch";
import config from 'config/index';

const apolloFetch = createApolloFetch({ uri: config.GRAPHQL_URL });

export default apolloFetch;