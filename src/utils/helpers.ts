export function optionalChaining(func) {
    try {
        return func();
    } catch (err) {
        return undefined;
    }
}