import { google } from 'googleapis';
import config from '../config/index';

const SCOPES= [
    'https://www.googleapis.com/auth/spreadsheets'
];

export const imaps = require('imap-simple');
export const imapsConfig = {
    imap: {
        user: config.imapUser,
        password: config.imapPassword,
        host: config.imapHost,
        port: 993,
        tls: true,
        authTimeout: 3000
    }
};

export const googleSheets = google.sheets('v4')

