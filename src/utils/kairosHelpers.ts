export const kairosCallback = (err, result) => {
	if (err) {
		console.log(err);
		throw new Error(JSON.stringify(err));
	} else {
		console.log(result.data);
		return result.data;
	}
};