import config from 'config/index';
const SDK = require('@serverless/event-gateway-sdk')

export default new SDK({
    url: config.slsGatewayUrl
})