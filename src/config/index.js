const dotenv = require("dotenv");

// require and configure dotenv, will load vars in .env in PROCESS.ENV
dotenv.config();

const config = {
	dev: {
		GRAPHQL_URL: "https://hyrecar-graphql-production.now.sh/internal",
		algoliaAppId: "5CX95NC9BW",
		slsGatewayUrl: "hyrecar-hyrecardev.slsgateway.com",
		kairosAppId: "2e8c2da3",
		kairosAppKey: "ef1dae99323ded99412d9a11f01634f8",
		accessKeyId: "AKIAJADFVLHXIJQX6OMQ",
		secretAccessKey: "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",
		googleSheetApiKey: "AIzaSyAPlPb-PB_r9IXmUAxACXFKcouqL9zPupA",

		imapUser: "lyftportal@hyrecar.com",
		imapPassword: "Lyftportal4$",
		imapHost: "imap.gmail.com"
	},
	staging: {
		GRAPHQL_URL: "https://hyrecar-graphql.now.sh/internal",
		algoliaAppId: "5CX95NC9BW",
		slsGatewayUrl: "hyrecar-hyrecardev.slsgateway.com",
		kairosAppId: "2e8c2da3",
		kairosAppKey: "ef1dae99323ded99412d9a11f01634f8",
		accessKeyId: "AKIAJADFVLHXIJQX6OMQ",
		secretAccessKey: "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",
		googleSheetApiKey: "AIzaSyAPlPb-PB_r9IXmUAxACXFKcouqL9zPupA",

		imapUser: "lyftportal@hyrecar.com",
		imapPassword: "Lyftportal4$",
		imapHost: "imap.gmail.com"
	},
	production: {
		GRAPHQL_URL: "https://hyrecar-graphql-production.now.sh/internal",
		algoliaAppId: "5CX95NC9BW",
		slsGatewayUrl: "hyrecar-hyrecardev.slsgateway.com",
		kairosAppId: "2e8c2da3",
		kairosAppKey: "ef1dae99323ded99412d9a11f01634f8",
		accessKeyId: "AKIAJADFVLHXIJQX6OMQ",
		secretAccessKey: "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",
		googleSheetApiKey: "AIzaSyAPlPb-PB_r9IXmUAxACXFKcouqL9zPupA",

		imapUser: "lyftportal@hyrecar.com",
		imapPassword: "Lyftportal4$",
		imapHost: "imap.gmail.com"
	},
	stage: process.env.SLS_STAGE,
	algoliaApiKey: process.env.SLS_ALGOLIA_API_KEY,
};
const flattenedConfig = {
	...config,
	...(config[process.env.STAGE] || config.dev)
};

module.exports.default = flattenedConfig;
module.exports.getEnvVars = () => flattenedConfig;
