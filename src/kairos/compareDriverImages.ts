import instance from "./kairosClient";
import { kairosCallback } from "utils/kairosHelpers";
import { optionalChaining } from "utils/helpers";

export const handler = async (event, ctx, cb) => {
	const { data } = event;
	try {
        const enrolledData = await enrollDriverPhoto(data);
        const verifiedData = await verifyDriverPhoto(data);

        // console.log(JSON.stringify(enrolledData));
        // console.log(" ");
        // console.log(" ");
        // console.log(" ");
        // console.log(JSON.stringify(verifiedData));
        const response = {
            statusCode: 200,
            body: JSON.stringify(verifiedData)
        }
        cb(null, response);
	} catch (err) {
		console.error(err);
	}
};


async function enrollDriverPhoto(data) {
    const {userImage, id} = data;
    const enrollData = {
        "image":userImage,
        "subject_id":id,
        "gallery_name":"users"
    };

    try{
        let res = await instance.post('http://api.kairos.com/enroll', enrollData);
        return res.data;
    }catch(err){
        console.error(err);
        throw new Error(err);
    }
};

async function verifyDriverPhoto(data) {
    const {licenseImage, id} = data;
    const verifyData = {
        "image":licenseImage,
        "subject_id":id,
        "gallery_name":"users"
    };

    try{
        let res = await instance.post('http://api.kairos.com/verify', verifyData)
        return res.data;
    }catch(err){
        console.error(err);
		throw new Error(err);
    }
};