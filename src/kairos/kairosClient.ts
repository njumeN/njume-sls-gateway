import config from "../config";
const axios = require('axios');
const instance = axios.create({
    headers: {
        'Content-Type': 'application/json',
        'app_id': config.kairosAppId,
        'app_key': config.kairosAppKey
    }
});

export default instance;
