import { s3, rekognition } from "./rekognitionClient";
import { rekognitionCallback } from "utils/rekognitionHelpers";
import { optionalChaining } from "utils/helpers";

export const handler = async (event, ctx, cb) => {
	const { data } = event;
	try {
        const userImagedData = await getImageObject(data.bucket, data.userImage);
        const licenseImagedData = await getImageObject(data.bucket, data.licenseImage);
        const comparedImageData = await compareImages(data, cb);
	} catch (err) {
		console.error(err);
	}
};

async function dataCallBack(data){
    return data ? data : '';
}

async function getImageObject(dataBucket, dataImage) {
    const imgData = s3.getObject({Bucket: dataBucket, Key: dataImage}, (err,userImageData) => {
        if(err){
            console.error(err, err.stack)
            throw new Error(err);
        }
    });
    return imgData;
};

async function compareImages(data, cb){
    const {bucket, userImage, licenseImage} = data;
    const compareParams = {
        SimilarityThreshold: 0, 
        SourceImage: {
            S3Object: {
                Bucket: bucket, 
                Name: userImage
            }
        }, 
        TargetImage: {
            S3Object: {
                Bucket: bucket, 
                Name: licenseImage
            }
        }
    };
    const compData = rekognition.compareFaces(compareParams, (err, compareData) => {
        if (err){ 
            console.error(err, err.stack);
            throw new Error(err);
        } 
        console.log(compareData);   
        const response = {
            statusCode: 200,
            body: JSON.stringify(compareData)
        }
        cb(null, response);
    });
    return compData;
}

