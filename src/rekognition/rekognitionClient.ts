import config from "../config";
const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});

export const rekognition = new AWS.Rekognition();
export const s3 = new AWS.S3(); 